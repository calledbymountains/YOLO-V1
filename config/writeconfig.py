import argparse

import yaml

parser = argparse.ArgumentParser(
    description='Code to generate the configuration file for YOLOV1')
parser.add_argument('--filename',
                    help='Full path of the file in which the configuration is to be stored.')

config = {
    'basenet': {
        'name': 'InceptionV4'
    },
    'grid_params': {
        'S': 7,
        'B': 2
    },
    'numclasses': 20,
    'data': {
        'train': {
            'path': '/local/ujjwal/tfrecord/reasonable/train',
            'batch_size': 4,
            'shuffle': True,
            'prefetch_count': 512
        },
        'test': {
            'path': '/local/ujjwal/tfrecord/reasonable/train',
            'batch_size': 8,
            'shuffle': False,
            'prefetch_count': 128
        }
    }
}


def writeyaml(filename=None):
    if filename is None:
        filename = './config.yaml'

    with open(filename, 'w') as outfile:
        yaml.dump(config, outfile, default_flow_stype=False)

    return None


if __name__ == '__main__':
    args = parser.parse_args()
    filename = args.filename
    writeyaml(filename)
