from nets.slimmodels import inception_v4, resnet_v1, resnet_v2
import tensorflow as tf
import logging
from colorama import Fore, Style

slim = tf.contrib.slim

VALID_ARCHS = [
    'InceptionV4',
    'ResNetV1-50',
    'ResNetV1-101',
    'ResNetV1-152',
    'ResNetV1-200',
    'ResNetV2-50',
    'ResNetV2-101',
    'ResNetV2-152',
    'ResNetV2-200'
]


def getbasenet(input_tensor=None, netname=None, is_training=False):
    """
    Based on the name supplied, returns the endpoints of one of the base network architectures.
    :param input_tensor: A Tensor of the form [batch_size, height, width, 3]
    :param netname: A string representing the name of the base network. Must be one from the list VALID_ARCHS
    :param is_training: A bool. True, if the network is intended for training. False otherwise.
    :return: A python dictionary containing the endpoints of the selected network architecture.
    """
    isvalid = netname in VALID_ARCHS
    if not isvalid:
        if netname is None:
            logging.FATAL(Fore.RED + Style.BRIGHT + 'No name of the base network was specified.'
                                                    ' This is a FATAL ERROR.')
        else:
            logging.FATAL(Fore.RED + Style.BRIGHT + 'The provided name of the base network ( {} ) was invalid.'
                                                    'The valid names are {}. THIS IS A FATAL '
                                                    'ERROR.'.format(netname, ','.join(VALID_ARCHS)))
        return None

    if input_tensor is None:
        logging.FATAL(Fore.RED + Style.BRIGHT + 'The input was not provided. It must be a tensor of the form'
                                                '[batch_size, height, width, 3]. THIS IS A FATAL ERROR.')
        return None

    if netname == 'InceptionV4':
        with slim.arg_scope(inception_v4.inception_v4_arg_scope()):
            _, endpoints = inception_v4.inception_v4(inputs=input_tensor, num_classes=0, is_training=is_training,
                                                     create_aux_logits=True, dropout_keep_prob=0.8)

    if netname == 'ResNetV1-50':
        with slim.arg_scope(resnet_v1.resnet_arg_scope()):
            _, endpoints = resnet_v1.resnet_v1_50(inputs=input_tensor, num_classes=0, is_training=is_training)

    if netname == 'ResNetV1-101':
        with slim.arg_scope(resnet_v1.resnet_arg_scope()):
            _, endpoints = resnet_v1.resnet_v1_101(inputs=input_tensor, num_classes=0, is_training=is_training)

    if netname == 'ResNetV1-152':
        with slim.arg_scope(resnet_v1.resnet_arg_scope()):
            _, endpoints = resnet_v1.resnet_v1_152(inputs=input_tensor, num_classes=0, is_training=is_training)

    if netname == 'ResNetV1-200':
        with slim.arg_scope(resnet_v1.resnet_arg_scope()):
            _, endpoints = resnet_v1.resnet_v1_200(inputs=input_tensor, num_classes=0, is_training=is_training)

    if netname == 'ResNetV2-50':
        with slim.arg_scope(resnet_v2.resnet_arg_scope()):
            _, endpoints = resnet_v2.resnet_v2_50(inputs=input_tensor, num_classes=0, is_training=is_training)

    if netname == 'ResNetV2-101':
        with slim.arg_scope(resnet_v2.resnet_arg_scope()):
            _, endpoints = resnet_v2.resnet_v2_101(inputs=input_tensor, num_classes=0, is_training=is_training)

    if netname == 'ResNetV2-152':
        with slim.arg_scope(resnet_v2.resnet_arg_scope()):
            _, endpoints = resnet_v2.resnet_v2_152(inputs=input_tensor, num_classes=0, is_training=is_training)

    if netname == 'ResNetV2-200':
        with slim.arg_scope(resnet_v2.resnet_arg_scope()):
            _, endpoints = resnet_v2.resnet_v2_200(inputs=input_tensor, num_classes=0, is_training=is_training)

    return endpoints
