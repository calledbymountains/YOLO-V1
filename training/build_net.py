import tensorflow as tf
from nets.basenet import getbasenet


def buildnet(input_tensor, basenet_name, basenet_endpoint, out_units, weight_decay, is_training=False):
    basenet = getbasenet(input_tensor=input_tensor,
                         netname=basenet_name,
                         is_training=is_training)
    out = basenet[basenet_endpoint]
    with tf.variable_scope('prediction_layer'):
        out = tf.layers.flatten(out, name='flattening')
        out_tensor = tf.layers.dense(inputs=out,
                                     units=out_units,
                                     kernel_regularizer=tf.contrib.layers.l2_regularizer(scale=weight_decay),
                                     name='prediction')

    return out_tensor
